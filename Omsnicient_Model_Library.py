import os
from _csv import writer
from pathlib import Path
import numpy as np
from colorsys import hsv_to_rgb
from matplotlib import pyplot as plt
from matplotlib.offsetbox import TextArea, VPacker, AnchoredOffsetbox
from numpy.linalg import norm as norm

#%% md
''' Notebook Independent Constant Variables
The model in this notebook have the following variables related to the flocking model:
-**N**= Number of individuals belonging to the flock.
-**a**= Radius of all individuals.
-**$k_\eta$**= Gain of the Wiener process representing rotational noise.
-**U**= Linear speed for all individuals (*this is set as a factor of a/$k_\eta$*).
-**$k_\odot$**= Gain of the attraction component of the model.
-**$k_{\parallel,\phi}$**= Gain of the alignment component of the model, azimuthal part.
-**$k_{\parallel,r}$**= Gain of the alignment component of the model, radial part.
*These gain will be aggregated to a single array* gains=$[k_\odot,k_{\parallel,\phi},k_{\parallel,r},k_\eta]$
-$\delta t$ Time step of the simulation.
-$\epsilon$ Blind spot of the visual field effect ($\epsilon$=0 perfect view)
'''
class Global_static_Variables:
    def __init__(self, _N, _DT, _EPSILON, _GAINS, _U,_GAINSPretty=[], _RADIUS=1):
        # Model Variables
        if not _GAINSPretty:
            _GAINSPretty=_GAINS
        self._N = _N
        self._RADIUS = _RADIUS
        self._DT = _DT
        self._EPSILON = _EPSILON
        self._U = _U
        self._GAINS = np.array(_GAINS)
        self._GAINSPretty = np.array(_GAINSPretty)[np.newaxis, :]
        self.NewDts = 0
        rng = np.random.default_rng()
        self._RNG=rng.integers(low=0, high=10000, size=1)
    def __str__(self):
        out = "\\V=%0.4f\\N=%04d\\dt=%0.4f\\%0.2f=%0.3f-%0.3f-%0.3f-%0.3f" % (self._U ,self._N, self._DT, self._EPSILON,
                                                                            self._GAINSPretty[0][0], self._GAINSPretty[0][1],
                                                                            self._GAINSPretty[0][1], self._GAINSPretty[0][2])
        return out
#%% md
''' Flock Related functions
These set of functions simulate the flock environment. All theres are the functions that better dynamics, kinematics, hardware in the loop, real physics simulators could replace.
#### updated_metric=add_metric(metric,value)
Miscelaneous function to add a value to a numpy array at the beginning and remove the last item on the array.
-*metric* the numpy array
-*value* the value to add
-**updated_metric** the new numpy array with the added value at the beginning
'''
#Flock Functions
def add_metric(metric,value):
    updated_metric=np.roll(metric,1,axis=0)
    updated_metric[0,:,:]=value
    return updated_metric
#%% md
''' Visual_Fields=Flock_Update_Visual_Field(Flock,Flock_Colors,Constants)
Function that takes the flock numpy array with the coordinates and heading of all the flock and returns a dictionary with each individual visual field centered at their heading
-*Flock* = [N x 3] numpy array with the x,y coordinates and the heading for all the individuals of the flock
-*Flock_Colors* = [N x 1] array with the HEX color values for the flock
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Flock_visual_field** = [N x k x 2] Dictionary with the all the visual fields for the current $\delta t$, rising edge and falling edge of each shade that an individual sees for all the flock.
'''
import warnings

def Flock_Update_Visual_Field(Flock,Flock_Colors,Constants):
    warnings.filterwarnings('ignore')
    Flock_visual_field={}
    for i,ind,name in zip(range(len(Flock)),Flock,Flock_Colors):
        flockwithout=np.delete(Flock,i,axis=0)
        relative_pos=flockwithout[:,:-1]-ind[:-1]
        theta=np.arctan2(relative_pos[:,1],relative_pos[:,0])-ind[-1]
        theta=np.mod(theta,2*np.pi)
        r=np.hypot(relative_pos[:,0],relative_pos[:,1])
        with np.errstate(divide='ignore'):
            phi=np.arcsin(Constants._RADIUS/r)
        if any(np.isnan(theta)):
            print(phi)
        phi=np.nan_to_num(phi, nan=np.pi-np.pi/100, posinf=np.pi-np.pi/100, neginf=np.pi-np.pi/100)
        angles_start=theta-phi
        angles_end=theta+phi
        pi_reflection=np.where(angles_start<=0)
        angles_start[pi_reflection]=angles_start[pi_reflection]+2*np.pi
        sorting_order=np.argsort(angles_start,kind='heapsort')
        angles_start=angles_start[sorting_order]
        angles_end=angles_end[sorting_order]
        Vt=[]
        if len(angles_start)>0:
            Vt.append([angles_start[0],angles_end[0]])
            for angle_start,angle_end in zip(angles_start[1:],angles_end[1:]):
                Curr_start=Vt[-1][0]
                Curr_end=Vt[-1][1]
                if angle_start<Curr_end:
                    if Curr_end < angle_end:
                        Vt[-1] = [Curr_start,angle_end]
                    else:
                        pass
                else:
                    Vt.append([angle_start,angle_end])
            if Vt[-1][1]<np.pi and Vt[-1][0]>np.pi:
                Vt[-1][1]+=2*np.pi
            if len(Vt)>3:
                if Vt[-2][1]-2*np.pi >=Vt[0][0]:
                    Vt[-2]=[Vt[-2][0],Vt[0][1]+2*np.pi]
                    Vt.pop(0)
            Flock_visual_field[name]=np.array(Vt)
        else:
            Flock_visual_field[name]=np.zeros([2,2])
    return Flock_visual_field
#%% md
''' [Flock,Flock_Colors,Flock_Omegas,Flock_visual_field]=Flock_Create(Constants)
Based on the Constants for the simulation this function returns:
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Flock** = [N x 3] numpy array with the x,y coordinates and the heading for all the individuals of the flock
-**Flock_Colors** = [N] array with the HEX color values for the flock
-**Flock_Omegas** = [N x 4] numpy array of zeros for the evaluation of the model
-**Flock_visual_field** = [N x k x 2] Dictionary with the all the visual fields for the current $\delta t$, rising edge and falling edge of each shade that an individual sees for all the flock.
'''
def Flock_Create(Constants):
    Flock_Colors=[]
    Flock=np.zeros([Constants._N,3])
    Flock[:,-1]=(np.random.random(Constants._N))*2*np.pi
    ##Position
    Flock[:,:-1]=np.random.random((Constants._N,2))*40*np.sqrt(Constants._N*Constants._RADIUS)
    for i in range(Constants._N):
        for j in range(Constants._N):
            if i!=j:
                relative_pos=Flock[i,:-1]-Flock[j,:-1]
                r=np.hypot(relative_pos[0],relative_pos[1])
                while r < (Constants._RADIUS + Constants._RADIUS * 0.1) * 2:
                    test = np.random.random(1)
                    if test < 0.25:
                        Flock[j,0] += 2 * Constants._RADIUS
                    if 0.25 <= test < 0.5:
                        Flock[j,0] -= 2 * Constants._RADIUS
                    if 0.5 <= test < 0.75:
                        Flock[j,1] -= 2 * Constants._RADIUS
                    if test >= 0.75:
                        Flock[j,1] += 2 * Constants._RADIUS
                    relative_pos=Flock[i,:-1]-Flock[j,:-1]
                    r=np.hypot(relative_pos[0],relative_pos[1])
        ##Colors
        h=200+(200+33*i)%80
        if i%20==0:
            h = 120
        s=1-(i%2)*0.3
        v=1-(i%25)*0.015
        rgb = hsv_to_rgb((h)%360/360, s,v)
        color = '#{:02x}{:02x}{:02x}'.format(*[int(255 * u) for u in rgb])
        j=0
        while color in Flock_Colors or color=='#ffffff':
            h=(h+j)%360
            v=1-((i+j)%2)*0.4
            s=1-((i+j)%25)*0.02
            rgb = hsv_to_rgb(h/360,v,s)
            j+=1
            color = '#{:02x}{:02x}{:02x}'.format(*[int(255 * u) for u in rgb])
        Flock_Colors.append(color)
    return [Flock,Flock_Colors]
#%% md
''' Updated_Flock = Flock_Update(Flock,Flock_Omegas,Constants)
This function updates the coordinates and the heading of the individuals of the flock to a new $\delta t$
-*Flock*= [N x 3] Numpy array with the current coordinates and heading for the flock
-*Flock_Omegas*= [N x 4] Numpy array with the current model omega output for the current $\delta t$
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Updated_Flock** = [N x 3] Numpy array with the new coordinates and heading for the flock
'''

def Flock_Update(Flock,Flock_Omegas,Constants):
    Updated_Flock = Flock
    Directions=np.mod(Flock[:,-1] +Constants._DT *np.sum(Constants._GAINS*Flock_Omegas,axis=1),2*np.pi)
    Updated_Flock[:,:-1]=Flock[:,:-1]+np.append(np.cos(Directions[:,np.newaxis]),np.sin(Directions[:,np.newaxis]),axis=1)*Constants._U*Constants._DT
    Updated_Flock[:,-1]=Directions
    return Updated_Flock
#%% md
''' Omega=Flock_Calculate_Omegas(Flock_Visual_Fields,Flock_Past_Visual_Fields,Flock_Directions,Prev_Omega,Constants)
-*Flock*= [N x 3] Numpy array with the current coordinates and heading for the flock
-*Constants* = [Global_static_Variables] All the simulation constants variables.
-**Omega**= [N x 3] Numpy array with the recently calculated model omega output for the current $\delta t$.
'''
def Flock_Calculate_Omegas(Flock,Constants):
    Omega=np.zeros([Constants._N,3])
    for i,ind in enumerate(Flock):
        FlockWIthOutI=np.delete(Flock,i,axis=0)
        Omega[i,0]=Model_Attraction(FlockWIthOutI,ind,Constants)
        Omega[i,1]=Model_Alignment(FlockWIthOutI,ind,Constants)
        Omega[i,2]=Weiner_Process(Constants)
        '''
        print('dtheta (dt*[K_odot*W_odot+K_parallel*W_parallel+eta]): ',Constants._DT *np.sum(Constants._GAINS*Omega[i,:]))
        print('\tW_odot\t\tW_parallel\teta')
        print(Omega[i,:])
        if np.min(A)<0:
            print('A Repulsion')
            print(A)
            print(Att)
            input()
        else:
            print('A Attraction')
            print(A)
            print(Att) '''


    return Omega
#%% md
''' Individual Functions
These functions are the model implementation, are done at an individual level and are the rule implementation for a drone/hardware implementation of the model. As such they do not use global information such as coordinates, instead they all use the current visual field. All these functions return floats (as the result is the omega for each model component).
#### Attraction_Omega=Model_Attraction(Flock,ind,Constants)
Implementation of:
$$ \omega_\odot = \frac{\sum_j  \frac{ A(\|x_i-x_j\|)\sin(\varphi_j)(1 + \epsilon\cos\varphi_j)}{\|x_i-x_j\|} }{\sum_j  \|\frac{ (1 + \epsilon\cos\varphi_j)}{\|x_i-x_j\|}\|} $$
-*Flock_without*= [N-1 x 3] Numpy array with the current coordinates and heading for the flock without the i-th individual
-*ind*= [3 x 1] Numpy array of the current coordinates and heading for the i-th individual
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Attraction_Omega**= [float] result of the distance model $ \omega_\odot$.
'''
#Model Implementation
def  Model_Attraction(Flock_without,ind,Constants):
    RelativePos=Flock_without[:,:-1]-ind[:-1]
    Angle=np.arctan2(RelativePos[:,1],RelativePos[:,0])-ind[-1]
    Distance=norm(RelativePos,axis=1)
    aux=Distance*(1+Constants._EPSILON*np.cos(Angle))
    Att=np.sin(Angle)*aux
    try:
        Attraction_Omega=float(np.sum(Att)/np.sum(np.abs(aux)))
    except:
        Attraction_Omega=0
    return Attraction_Omega
#%% md
''' #### A=Model_Repulsion(Distance)
Implementation of:
$$ A(R)=A
    \begin{cases}
        -1 & \text{if } R \, <a\\
        1 & \text{ if } R \, \geq a
    \end{cases} $$
-*Distance*= [N-1 x 1] Numpy array of all the distances between i-th individual and the rest of the flock
-**A**= [[N-1 x 1] Numpy array result of the model $ A(R)$.
'''
def Model_Repulsion(Distance,Constants):
    A=Distance.copy()
    attract=np.where(Distance>=2*Constants._RADIUS)
    repel=np.where(Distance<2*Constants._RADIUS)
    A[repel]=1
    A[attract]=1
    return A
#%% md
''' #### Alignment_Omega=Model_Alignment_Phi(Flock_without,ind,Constants)
Implementation of:
$$ \omega_{\parallel}  =
     \frac{\sum_j (e_i \times e_j) (1 + \epsilon\cos\varphi_k)}{  \sum_j \left | (1 + \epsilon\cos\varphi_k) \right |} $$
-*Flock_without*= [L x 1] Numpy array of all the middle retinal positions of the shades that generate V_phi seen by the i-th individual
-*ind* = [L x 1] Numpy array of the resulting OpticFlow azimuthal component that approximates the relative velocity seen on the visual field of the i-th individual.
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Attraction_Omega**= [float] result of the model $ \omega_{\parallel,\phi}$..
'''
def  Model_Alignment(Flock_without,ind,Constants):
    RelativePos=Flock_without[:,:-1]-ind[:-1]
    Distance = norm(RelativePos, axis=1)
    Phi=np.arctan2(RelativePos[:,1],RelativePos[:,0])-ind[-1]
    ei=np.array([np.cos(ind[-1]),np.sin(ind[-1])])
    ej=np.array([np.cos(Flock_without[:,-1]),np.sin(Flock_without[:,-1])]).transpose()

    Ali=(np.cross(ei,ej)/np.power(Distance,2))*(1+Constants._EPSILON*np.cos(Phi))
    aux=np.abs((1+Constants._EPSILON*np.cos(Phi))/np.power(Distance,2))
    Alignment_Omega=np.sum(Ali)/np.sum(aux)
    return Alignment_Omega
#%% md
''' Weiner_Omega=Weiner_Process(Constants)
Implementation of $\eta$
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**Weiner_Omega**=[float] result of the model $\eta$
'''
def Weiner_Process(Constants):
    Weiner_Omega=np.random.normal(loc=0, scale=1/np.sqrt(Constants._DT))
    return Weiner_Omega

#Flock Metric Functions
'''
## Flock Metric Functions
These are functions that implement the metrics used to describe any flock behaviour as a whole.
#### P=Flock_Calculate_P(Flock_Direction,Constants)
Implementation of:
$$ P =\| \overline{\boldsymbol{e}_i}\|$$
-**Flock_Direction** = [N x 1] numpy array with  the heading for all the individuals of the flock
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**P**= [float] metric result
'''

def Flock_Calculate_P(Flock_Direction,Constants):
    eix=np.cos(Flock_Direction)
    eiy=np.sin(Flock_Direction)
    P=norm([np.sum(eix),np.sum(eiy)])
    P=P/Constants._N
    return P
'''
#### [Center,Dir]=Flock_Calculate_Center(Flock)
Calculate the centre  and the heading of the flock
-*Flock* = [N x 3] numpy array with the x,y coordinates and the heading for all the individuals of the flock
-**Dir** = [float] heading of the flock
-**Center**= x,y coordinate of the weighted center of the flock
'''
def Flock_Calculate_Center(Flock):
    Center=np.mean(Flock[:,:-1],axis=0)
    eix=np.cos(Flock[:,-1])
    eiy=np.sin(Flock[:,-1])
    fx=np.mean(eix)
    fy=np.mean(eiy)
    Dir=np.mod(np.arctan2(fy,fx),2*np.pi)
    return [Center,Dir]
'''
#### M=Flock_Calculate_M(Flock,Center)
Implementation of:
$$M = \| \overline{\boldsymbol{y}_i \times \boldsymbol{e}_i} \| $$
$\boldsymbol{y}_i= (\boldsymbol{x}_i- \overline{ \boldsymbol{x}_i})/\|\boldsymbol{x}_i- \overline{ \boldsymbol{x}_i}\|$

-*Center*=[2 x 1] x,y coordinate of the center of the flock
-*Flock* = [N x 3] numpy array with the x,y coordinates and the heading for all the individuals of the flock
-**M**= [float] Result of the metric
'''
def Flock_Calculate_M(Flock,Center):
    vec_cent_2_ind=Flock[:,:-1]-Center
    eix=np.cos(Flock[:,-1])
    eiy=np.sin(Flock[:,-1])
    dir_vec=np.concatenate((eix[:,np.newaxis],eiy[:,np.newaxis]),axis=1)
    all_M=np.cross(vec_cent_2_ind,dir_vec)/norm(vec_cent_2_ind,axis=1)
    M=np.abs(np.mean(all_M))
    return M
#%% md
''' S = Flock_Calculate_S(Flock_Position,Center,Constants)
implementation of:
$$S = \left(\overline{\|\boldsymbol{x}_i- \overline{ \boldsymbol{x}_i}\|^2} / a^2\right)^{1/2}$$
-*Flock_Position* = [N x 3] numpy array with the x,y coordinates and the heading for all the individuals of the flock
-*Center*=[2 x 1] x,y coordinate of the center of the flock
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**S**=[float] metric result
'''
def Flock_Calculate_S(Flock_Position,Center,Constants):
    S=np.sqrt(np.mean(np.square(norm(Flock_Position-Center,axis=1)))/(Constants._RADIUS**2))
    return S
#%% md
''' H = Flock_Calculate_S(Flock_visual_field,Constants)
implementation of:
$$H=\frac{\sum_i{\sum_k \frac{\Delta \phi_{i,k}}{2\pi}}}{N}$$
-*Flock_Visual_Fields* = [N x k x 2] Dictionary with the visual field for the current $\delta t$, rising edge and falling edge of each shade that an individual sees.
-*Constants* = [Global_static_Variables] All the simulation constants variables
-**S**=[float] metric result
'''
def Flock_Calculate_H(Flock_visual_field,Constants):
    Flock_H=np.zeros(Constants._N)
    for ind,key in enumerate(Flock_visual_field):
        Flock_H[ind]=np.sum(np.abs(Flock_visual_field[key][:,1]-Flock_visual_field[key][:,0]))/(2*np.pi)
    H=np.mean(Flock_H)
    return H



def Get_Snapshot_at_dt(Flock,Metrics,Flock_History,Flock_Colors,Constants):

    f,ax = plt.subplots(2,1,
            gridspec_kw={'height_ratios':[1,0.2]},figsize=(15,18))
    for k in range(len(Flock_History)):
        k_coords = Flock_History[k, :, :-1]
        if not (np.mean(k_coords) == 0):
            pass
            for color, Pos in zip(Flock_Colors, k_coords):
                if k == 0:
                    pass
                else:
                    ind_circle = plt.Circle((Pos[0], Pos[1]), radius=Constants._RADIUS / 10, color=color,
                                            alpha=(1 * len(Flock_History) - k) / (1.5 * len(Flock_History)))
                    ax[0].add_patch(ind_circle)
                    del (ind_circle)
    for ind, ind_color, in zip(Flock, Flock_Colors):
        ind_dir = ind[-1]
        ind_circle = plt.Circle((ind[0], ind[1]), radius=Constants._RADIUS, color=ind_color, alpha=1)
        ax[0].add_patch(ind_circle)
        ind_circle = plt.Circle(
            (ind[0] + Constants._RADIUS * np.cos(ind_dir), ind[1] + Constants._RADIUS * np.sin(ind_dir)),
            radius=Constants._RADIUS // 2, color=ind_color, alpha=1)
        ax[0].add_patch(ind_circle)
        del (ind_circle)
    ax[0].axis('off')
    [xm, xM, ym, yM] = ax[0].axis('equal')
    sc2 = plt.Rectangle(((xm + xM) / 2 - 10 * Constants._RADIUS, ym - (yM-ym)//50), 10 * Constants._RADIUS,
                        (yM-ym)//50, linewidth=0.1, color='0.2')
    sc1 = plt.Rectangle(((xm + xM) / 2, ym - (yM-ym)//50), 10 * Constants._RADIUS, (yM-ym)//50,
                        linewidth=0.1, color='0.8')
    sc3 = plt.Rectangle(((xm + xM) / 2 + 10 * Constants._RADIUS, ym - (yM-ym)//50), 10 * Constants._RADIUS,
                        (yM-ym)//50, linewidth=0.1, color='0.2')
    ax[0].add_patch(sc1)
    ax[0].add_patch(sc2)
    ax[0].add_patch(sc3)
    ax[0].axis('equal')
    x = np.linspace(0, len(Metrics[:, 0]) * Constants._DT * Constants._U / Constants._RADIUS, num=len(Metrics[:, 0]),
                    endpoint=True)

    ax[1].plot(x, Metrics[:,2], 'k-', label='H')
    ax[1].plot(x, Metrics[:, 0], 'b-', label='P')
    ax[1].plot(x, Metrics[:, 1], 'r-', label='M')
    ax[1].set_ylim(bottom=0, top=1.05)
    ax[1].set_xlabel('Normalized Time [s]')
    ybox1 = TextArea("    Milling    ", textprops=dict(color="r", size=12,rotation=90,ha='left',va='bottom'))
    ybox3 = TextArea("Opacity   ", textprops=dict(color="k", size=12,rotation=90,ha='left',va='bottom'))
    ybox2 = TextArea("Polarization    ",     textprops=dict(color="b", size=12,rotation=90,ha='left',va='bottom'))

    ybox = VPacker(children=[ybox3, ybox2, ybox1],align="bottom", pad=0, sep=0)

    anchored_ybox = AnchoredOffsetbox(loc=10, child=ybox, pad=0., frameon=False, bbox_to_anchor=(-0.08, 0.4),
                                      bbox_transform=ax[1].transAxes, borderpad=0.)

    ax[1].add_artist(anchored_ybox)
    name = os.getcwd()+'\\Sim Files Traditional\\'+str(Constants) + '\\Images\\'
    Path(name).mkdir(parents=True, exist_ok=True)
    name = os.getcwd()+'\\Sim Files Traditional\\'+str(Constants) + '\\Images\\Im %012.4f.png'%(Constants.NewDts*Constants._DT)
    f.savefig(name, bbox_inches=None, pad_inches='tight',
                transparent=True, edgecolor='auto')
    plt.close()
def Get_Snapshot_at_end(Flock,Metrics,Flock_History,Flock_Colors,Constants):

    f,ax = plt.subplots(2,1,
            gridspec_kw={'height_ratios':[1,0.2]},figsize=(15,18))
    for k in range(len(Flock_History)):
        k_coords = Flock_History[k, :, :-1]
        if not (np.mean(k_coords) == 0):
            pass
            for color, Pos in zip(Flock_Colors, k_coords):
                if k == 0:
                    pass
                else:
                    ind_circle = plt.Circle((Pos[0], Pos[1]), radius=Constants._RADIUS / 10, color=color,
                                            alpha=(1 * len(Flock_History) - k) / (1.5 * len(Flock_History)))
                    ax[0].add_patch(ind_circle)
                    del (ind_circle)
    for ind, ind_color, in zip(Flock, Flock_Colors):
        ind_dir = ind[-1]
        ind_circle = plt.Circle((ind[0], ind[1]), radius=Constants._RADIUS, color=ind_color, alpha=1)
        ax[0].add_patch(ind_circle)
        ind_circle = plt.Circle(
            (ind[0] + Constants._RADIUS * np.cos(ind_dir), ind[1] + Constants._RADIUS * np.sin(ind_dir)),
            radius=Constants._RADIUS // 2, color=ind_color, alpha=1)
        ax[0].add_patch(ind_circle)
        del (ind_circle)
    ax[0].axis('off')
    [xm, xM, ym, yM] = ax[0].axis('equal')
    sc2 = plt.Rectangle(((xm + xM) / 2 - 10 * Constants._RADIUS, ym - (yM-ym)//50), 10 * Constants._RADIUS,
                        (yM-ym)//50, linewidth=0.1, color='0.2')
    sc1 = plt.Rectangle(((xm + xM) / 2, ym - (yM-ym)//50), 10 * Constants._RADIUS, (yM-ym)//50,
                        linewidth=0.1, color='0.8')
    sc3 = plt.Rectangle(((xm + xM) / 2 + 10 * Constants._RADIUS, ym - (yM-ym)//50), 10 * Constants._RADIUS,
                        (yM-ym)//50, linewidth=0.1, color='0.2')
    ax[0].add_patch(sc1)
    ax[0].add_patch(sc2)
    ax[0].add_patch(sc3)
    ax[0].axis('equal')
    x = np.linspace(0, len(Metrics[:, 0]) * Constants._DT * Constants._U / Constants._RADIUS, num=len(Metrics[:, 0]),
                    endpoint=True)
    ax[1].plot(x, Metrics[:,2], 'k-', label='H')
    ax[1].plot(x, Metrics[:, 0], 'b-', label='P')
    ax[1].plot(x, Metrics[:, 1], 'r-', label='M')
    ax[1].set_ylim(bottom=0, top=1.05)
    ax[1].set_xlabel('Normalized Time [s]')
    ybox1 = TextArea("    Milling    ", textprops=dict(color="r", size=12,rotation=90,ha='left',va='bottom'))
    ybox3 = TextArea("Opacity   ", textprops=dict(color="k", size=12,rotation=90,ha='left',va='bottom'))
    ybox2 = TextArea("Polarization    ",     textprops=dict(color="b", size=12,rotation=90,ha='left',va='bottom'))
    ybox = VPacker(children=[ybox3, ybox2, ybox1],align="bottom", pad=0, sep=0)
    anchored_ybox = AnchoredOffsetbox(loc=10, child=ybox, pad=0., frameon=False, bbox_to_anchor=(-0.08, 0.4),
                                      bbox_transform=ax[1].transAxes, borderpad=0.)
    rng = np.random.default_rng()
    ax[1].add_artist(anchored_ybox)
    name = os.getcwd() + '\\Sim Files Traditional\\' + str(Constants)
    Path(name).mkdir(parents=True, exist_ok=True)
    name = os.getcwd() + '\\Sim Files Traditional\\' + str(Constants) + '\\Ending' + str(
        Constants._RNG) + '.png'
    f.savefig(name, bbox_inches=None, pad_inches='tight',
                transparent=True, edgecolor='auto')
    plt.close()
def Misc_save_row_to_csv(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)
def Flock_Save(Constants, Metrics, _HISTORY, Flock, Flock_Colors, Flock_Omegas):


    aux=np.mean(Metrics[-_HISTORY:, :], axis=0)
    file_name = os.getcwd() + '\\Sim Files Traditional' + str(Constants)+"\\LastDt"+ str(
        Constants._RNG) +".csv"
    with open(file_name, 'w+', newline='') as write_obj:
        pass
    Data=[aux[0],aux[1],aux[2]]
    for Pos,Dir,Color,Omega in zip(Flock[:,:-1],Flock[:,-1],Flock_Colors,Flock_Omegas):
        Data.append(Color)
        Data.append(Pos[0])
        Data.append(Pos[1])
        Data.append(Dir)
        Data.append(Omega[0])
        Data.append(Omega[1])
        Data.append(Omega[2])
    Misc_save_row_to_csv(file_name,Data)