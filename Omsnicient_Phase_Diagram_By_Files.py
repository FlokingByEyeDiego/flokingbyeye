import os
import glob
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
def fmt(x):
    s = f"{x:.2f}"
    if s.endswith("0"):
        s = f"{x:.2f}"
    return rf"{s} " if plt.rcParams["text.usetex"] else f"{s} "
olders=[]
bases=[]
names=[]
filenames={}
Simulations={}
ali={}
att={}
epsilon={}
font_size=20
Fig_size=20
pssatt=[]
pssali=[]
for i in range(0,11,2):
    pssatt.append(i/50)
    pssatt.append((1+i)/50)
for i in range(0,10+1):
    pssali.append(i/50)
    #pssali.append((1+i)/50)
for root, dirs, files in os.walk(os.getcwd(), topdown=False):
    for file in files:
        if 'Last' in file and 'onal\\' in root:
            aux=root.split('\\')
            _U=float(aux[-4].split('=')[-1])
            _DT=float(aux[-2].split('=')[-1])
            _N=float(aux[-3].split('=')[-1])
            [epsi,aux]=aux[-1].split('=')
            [stratt, strali,_, streta] = aux.split('-')
            Keta=float(streta)
            if float(stratt) in pssatt[:-1] and float(strali) in pssali :
                try:
                    if float(epsi) not in epsilon[_U,_N,_DT,Keta]:
                        epsilon[_U,_N,_DT,Keta].append(float(epsi))
                except KeyError:
                    epsilon[_U, _N, _DT,Keta]=[]
                    epsilon[_U,_N,_DT,Keta].append(float(epsi))
                [stratt, strali,_,  streta] = aux.split('-')
                try:
                    if float(stratt) not in att[_U,_N,_DT,Keta,float(epsi)]:
                        att[_U,_N,_DT,Keta,float(epsi)].append(float(stratt))
                except KeyError:
                    att[_U,_N,_DT,Keta,float(epsi)]=[]
                    att[_U,_N,_DT,Keta,float(epsi)].append(float(stratt))
                try:
                    if float(strali) not in ali[_U,_N,_DT,Keta,float(epsi)]:
                        ali[_U,_N,_DT,Keta,float(epsi)].append(float(strali))
                except KeyError:
                    ali[_U,_N,_DT,Keta,float(epsi)]=[]
                    ali[_U,_N,_DT,Keta,float(epsi)].append(float(strali))
                try:
                    filenames[_U, _N, _DT,Keta,float(epsi),float(stratt),float(strali)].append(root+'\\'+file)
                except KeyError:
                    filenames[_U, _N, _DT,Keta,float(epsi),float(stratt),float(strali)]=[]
                    filenames[_U, _N, _DT,Keta,float(epsi),float(stratt),float(strali)].append(root+'\\'+file)
for globalvar in epsilon:
    _U=globalvar[0]
    _N=globalvar[1]
    _DT=globalvar[2]
    Keta=globalvar[3]
    for epsi in epsilon[_U,_N,_DT,Keta]:
        Phase_Map_Metric = np.zeros([len(att[_U,_N,_DT,Keta,epsi]), len(ali[_U,_N,_DT,Keta,epsi]), 3])
        Phase_Map_Metric_mean = np.zeros([len(att[_U,_N,_DT,Keta,epsi]), len(ali[_U,_N,_DT,Keta,epsi]), 3])
        att[_U, _N, _DT, Keta, epsi].sort()
        ali[_U, _N, _DT, Keta, epsi].sort()
        for stratt in att[_U,_N,_DT,Keta,epsi]:
            for strali in ali[_U,_N,_DT,Keta,epsi]:
                Qexp=len(filenames[_U, _N, _DT,Keta,epsi,stratt,strali])
                for file in filenames[_U, _N, _DT,Keta,epsi,stratt,strali]:
                    with open(file,'r') as f:
                        aux=f.readlines()
                    [P,M,O]=aux[0].split(',')[:3]
                    Phase_Map_Metric[att[_U,_N,_DT,Keta,epsi].index(stratt),ali[_U,_N,_DT,Keta,epsi].index(strali)]+=\
                        [float(P)/Qexp,float(M)/Qexp,np.abs(float(O))/Qexp]
        for stratt in att[_U,_N,_DT,Keta,epsi]:
            for strali in ali[_U,_N,_DT,Keta,epsi]:
                Qexp=len(filenames[_U, _N, _DT,Keta,epsi,stratt,strali])
                for file in filenames[_U, _N, _DT,Keta,epsi,stratt,strali]:
                    with open(file,'r') as f:
                        aux=f.readlines()
                    [P,M,O]=aux[0].split(',')[:3]
                    [Pm,Mm,Om]=Phase_Map_Metric[att[_U, _N, _DT,Keta, epsi].index(stratt), ali[_U, _N, _DT,Keta, epsi].index(strali)]
                    Phase_Map_Metric_mean[att[_U,_N,_DT,Keta,epsi].index(stratt),ali[_U,_N,_DT,Keta,epsi].index(strali)]+=\
                        [(float(P)-Pm)**2/Qexp,(float(M)-Mm)**2/Qexp,(np.abs(float(O))-np.abs(Om))**2/Qexp]

        image = np.zeros((len(att[_U,_N,_DT,Keta,epsi]), len(ali[_U,_N,_DT,Keta,epsi]), 3))

        image[:, :, 2] = Phase_Map_Metric[:, :, 0]
        image[:, :, 0] = Phase_Map_Metric[:, :, 1]
        for i in range(len(att[_U,_N,_DT,Keta,epsi])):
            for j in range(len(ali[_U,_N,_DT,Keta,epsi])):
                image[i, j, 1] = np.max([1+image[i, j, 1]-(image[i, j, 0] +image[i, j, 2]),0])
        fig, ax = plt.subplots(1, 1,figsize=(Fig_size,Fig_size))
        ax.set_yticks(range(len(Phase_Map_Metric[ :, 0, 0])))
        ax.set_xticks(range(len(Phase_Map_Metric[0, :, 0])))
        xticks = [''] * len(Phase_Map_Metric[0, :, 0])
        xticks[0] = ali[_U,_N,_DT,Keta,epsi][0]
        xticks[int(len(xticks) / 4)] = ali[_U,_N,_DT,Keta,epsi][int(len(xticks) / 4)]
        xticks[2 * int(len(xticks) / 4)] = ali[_U,_N,_DT,Keta,epsi][2 * int(len(xticks) / 4)]
        xticks[3 * int(len(xticks) / 4)] = ali[_U,_N,_DT,Keta,epsi][3 * int(len(xticks) / 4)]
        xticks[-1] = ali[_U,_N,_DT,Keta,epsi][-1]
        ax.set_xticklabels(xticks,fontsize=font_size)
        yticks = [''] * len(Phase_Map_Metric[:,0, 0])
        yticks[0] = att[_U,_N,_DT,Keta,epsi][0]
        yticks[int(len(yticks) / 4)] = att[_U,_N,_DT,Keta,epsi][int(len(yticks) / 4)]
        yticks[2 * int(len(yticks) / 4)] = att[_U,_N,_DT,Keta,epsi][2 * int(len(yticks) / 4)]
        yticks[3 * int(len(yticks) / 4)] = att[_U,_N,_DT,Keta,epsi][3 * int(len(yticks) / 4)]
        yticks[-1] = att[_U,_N,_DT,Keta,epsi][-1]
        ax.set_xlabel('Alignment',fontsize=font_size)
        ax.set_ylabel('Atraction',fontsize=font_size)
        ax.set_yticklabels(yticks,fontsize=font_size)
        name = os.getcwd() +"\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f.png" % (_U ,_N, _DT,Keta, epsi)
        print(name)
        plt.imshow(image, origin='lower')
        fig.savefig(name, bbox_inches=None, pad_inches='tight',transparent=True, edgecolor='auto')
        plt.close()
        f, ax = plt.subplots(2, 3,
                             gridspec_kw={'height_ratios': [1, 0.1]}, figsize=(2.5*Fig_size,Fig_size))
        f.subplots_adjust(hspace=0.3)
        f.subplots_adjust(wspace=0.1)
        values = Phase_Map_Metric_mean[ :, :, 0]
        g1 = sns.heatmap(values, cmap="Blues", cbar_ax=ax[1, 0], ax=ax[0, 0], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 0].tick_params(labelsize=font_size)
        g1.invert_yaxis()
        g1.set_yticks(range(len(Phase_Map_Metric[ :,0, 0])))
        g1.set_xticks(range(len(Phase_Map_Metric[ 0,:, 0])))
        g1.set_xticklabels(xticks,fontsize=font_size)
        g1.set_xlabel('Alignment',fontsize=font_size)
        g1.set_ylabel('Atraction',fontsize=font_size)
        g1.set_yticklabels(yticks,fontsize=font_size,rotation=0)
        g1.set_title("P", fontsize=font_size)
        values = Phase_Map_Metric_mean[ :, :, 1]
        g2 = sns.heatmap(values, cmap="Reds", cbar_ax=ax[1, 1], ax=ax[0, 1], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 1].tick_params(labelsize=font_size)
        g2.invert_yaxis()
        g2.set_xticks(range(len(Phase_Map_Metric[0,:, 0])))
        g2.set_yticks(range(len(Phase_Map_Metric[:,0, 0])))
        g2.set_xticklabels(xticks, fontsize=font_size)
        g2.set_xlabel('Alignment', fontsize=font_size)
        g2.set_title("M", fontsize=font_size)

        values = Phase_Map_Metric_mean[ :, :, 2]
        g3 = sns.heatmap(values, cmap="Greys", ax=ax[0, 2], cbar_ax=ax[1, 2], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 2].tick_params(labelsize=font_size)
        g3.invert_yaxis()
        g3.set_xticks(range(len(Phase_Map_Metric[ 0,:, 0])))
        g3.set_yticks(range(len(Phase_Map_Metric[:,0, 0])))
        g3.set_xticklabels(xticks, fontsize=font_size)
        g3.set_xlabel('Alignment', fontsize=font_size)
        g3.set_title("O", fontsize=font_size)
        f.suptitle("Metric Variance", y=1.0, fontsize=font_size)
        name = os.getcwd() +"\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f Variance.png" % (_U ,_N, _DT,Keta, epsi)
        f.savefig(name, bbox_inches=None, pad_inches='tight',
                    transparent=True, edgecolor='auto')
        plt.close()
        f1, ax = plt.subplots(2, 3,
                             gridspec_kw={'height_ratios': [1, 0.1]}, figsize=(2.5*Fig_size,Fig_size))
        f1.subplots_adjust(hspace=0.3)
        f1.subplots_adjust(wspace=0.1)
        values = Phase_Map_Metric[:, :, 0]
        g1 = sns.heatmap(values, cmap="Blues", cbar_ax=ax[1, 0], ax=ax[0, 0], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 0].tick_params(labelsize=font_size)
        g1.invert_yaxis()
        g1.set_xticks(range(len(Phase_Map_Metric[0, :, 0])))
        g1.set_yticks(range(len(Phase_Map_Metric[:, 0, 0])))
        g1.set_xticklabels(xticks, fontsize=font_size)
        g1.set_xlabel('Alignment', fontsize=font_size)
        g1.set_ylabel('Atraction', fontsize=font_size)
        g1.set_yticklabels(yticks, fontsize=font_size,rotation=0)
        g1.set_title("P", fontsize=font_size)
        values = Phase_Map_Metric[ :, :, 1]
        g2 = sns.heatmap(values, cmap="Reds", cbar_ax=ax[1, 1], ax=ax[0, 1], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 1].tick_params(labelsize=font_size)
        g2.invert_yaxis()
        g2.set_xticks(range(len(Phase_Map_Metric[0, :, 0])))
        g2.set_yticks(range(len(Phase_Map_Metric[:, 0, 0])))
        g2.set_xticklabels(xticks, fontsize=font_size)
        g2.set_xlabel('Alignment', fontsize=font_size)
        g2.set_title("M", fontsize=font_size)

        values = Phase_Map_Metric[ :, :, 2]
        g3 = sns.heatmap(values,cmap="Greys", ax=ax[0, 2], cbar_ax=ax[1, 2], cbar_kws={"orientation": "horizontal"}, vmin=0, vmax=1)
        ax[1, 2].tick_params(labelsize=font_size)
        g3.invert_yaxis()
        g3.set_xticks(range(len(Phase_Map_Metric[ 0, :, 0])))
        g3.set_yticks(range(len(Phase_Map_Metric[:, 0, 0])))
        g3.set_xticklabels(xticks, fontsize=font_size)
        g3.set_xlabel('Alignment', fontsize=font_size)
        g3.set_title("O", fontsize=font_size)
        f1.suptitle("Metrics", y=1.0, fontsize=font_size)
        name = os.getcwd() +"\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f Separated.png" % (_U ,_N, _DT,Keta, epsi)
        f1.savefig(name, bbox_inches=None, pad_inches='tight',
                  transparent=True, edgecolor='auto')
        plt.close()
        fig, ax = plt.subplots(1, 1,figsize=(Fig_size,Fig_size))
        C_levels=[0.2,0.5,0.7,1]
        ax.contourf(Phase_Map_Metric[:, :, 2],levels=C_levels, colors="White", alpha=0.0)
        CS=ax.contour(Phase_Map_Metric[:, :, 2],levels=C_levels, colors="White",linewidths=5)
        ax.clabel(CS, CS.levels, inline=True, fmt=fmt, fontsize=font_size//2,rightside_up=True)
        ax.set_yticks(range(len(Phase_Map_Metric[:, 0, 2])))
        ax.set_xticks(range(len(Phase_Map_Metric[0, :, 2])))
        ax.set_xticklabels(xticks, fontsize=font_size)
        ax.set_xlabel('Alignment', fontsize=font_size)
        ax.set_ylabel('Atraction', fontsize=font_size)
        ax.set_yticklabels(yticks, fontsize=font_size)
        name = os.getcwd() + "\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f Contours.png" % (
        _U, _N, _DT,Keta, epsi)
        plt.imshow(image, origin='lower')
        fig.savefig(name, bbox_inches=None, pad_inches='tight', transparent=True, edgecolor='auto')

        fig, ax = plt.subplots(1, 1,figsize=(Fig_size,Fig_size))
        ax.contourf(Phase_Map_Metric[:, :, 2], cmap="Greys", alpha=0.2)
        CS=ax.contour(Phase_Map_Metric[:, :, 2],cmap="Greys",linewidths=5)
        ax.clabel(CS, CS.levels, inline=True, fmt=fmt, fontsize=font_size//2,rightside_up=True)
        ax.set_yticks(range(len(Phase_Map_Metric[:, 0, 2])))
        ax.set_xticks(range(len(Phase_Map_Metric[0, :, 2])))
        ax.set_xticklabels(xticks, fontsize=font_size)
        ax.set_xlabel('Alignment', fontsize=font_size)
        ax.set_ylabel('Atraction', fontsize=font_size)
        ax.set_yticklabels(yticks, fontsize=font_size)
        name = os.getcwd() + "\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f AutoContours.png" % (
        _U, _N, _DT,Keta, epsi)
        plt.imshow(image, origin='lower')
        fig.savefig(name, bbox_inches=None, pad_inches='tight', transparent=True, edgecolor='auto')


        fig, ax = plt.subplots(1, 1,figsize=(Fig_size,Fig_size))
        C_levels=[0.2,0.5,0.7,1]
        ax.contourf(Phase_Map_Metric[:, :, 2],levels=C_levels, colors="White", alpha=0.0)
        CS=ax.contour(Phase_Map_Metric[:, :, 2],levels=C_levels, colors="White",linewidths=10)
        C_levels=[0.5,1.1]
        ax.contourf(Phase_Map_Metric[:, :, 0], levels=C_levels, colors="White", alpha=0.0, linestyles="dashed")
        CS = ax.contour(Phase_Map_Metric[:, :, 0], levels=C_levels, colors="White", linewidths=10, linestyles="dashed")
        ax.contourf(Phase_Map_Metric[:, :, 1], levels=C_levels, colors="White", alpha=0.0, linestyles="dotted")
        CS = ax.contour(Phase_Map_Metric[:, :, 1], levels=C_levels, colors="White", linewidths=10, linestyles="dotted")
        ax.set_yticks(range(len(Phase_Map_Metric[:, 0, 2])))
        ax.set_xticks(range(len(Phase_Map_Metric[0, :, 2])))
        ax.set_xticklabels(xticks, fontsize=font_size)
        ax.set_xlabel('Alignment', fontsize=font_size)
        ax.set_ylabel('Atraction', fontsize=font_size)
        ax.set_yticklabels(yticks, fontsize=font_size)
        name = os.getcwd() + "\\Images\\Original Images\\Traditional U=%0.4f N %04d dt %0.4f keta %0.4f e%0.2f 1Contours.png" % (
        _U, _N, _DT,Keta, epsi)
        plt.imshow(image, origin='lower')
        fig.savefig(name, bbox_inches=None, pad_inches='tight', transparent=True, edgecolor='auto')