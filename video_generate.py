import os
import glob
import cv2
from pathlib import Path
from tkinter import filedialog as fd

folders = []
filetypes = (
    ('Global File', 'Global Metrics.csv'),
    ('CSV Files', '*.csv'),
    ('All files', '*.*')
)
for root, dirs, files in os.walk(os.getcwd(), topdown=False):
    for name in dirs:
        if name == "Images" and "al" in root:
            folders.append(os.path.join(root, name))

print(folders[-1].split('\\')[-6:-1])
video_base_folder = os.path.join(os.getcwd(), "All Videos")


for folder in folders:
    [Type,V,N,dt,Ks]=folder.split('\\')[-6:-1]
    img_array = []
    e=Ks.split('=')[0]
    [odot,parallel,_,eta]=Ks.split('=')[1].split('-')
    video_folder = os.path.join(video_base_folder, Type)
    Path(video_folder).mkdir(parents=True, exist_ok=True)
    name = "\\"+N+" "+dt+" e="+e+" odot="+odot+" parallel="+parallel+" eta="+eta+".mp4"
    # for filename in glob.glob(folder+'/*.png'):
    #    if ".1" in filename.split("Im ")[1] or ".2" in filename.split("Im ")[1] or ".3" in filename.split("Im ")[1]:
    #    aux=filename.split("Im ")
    #    auxname=int(aux[1].split(".")[0])
    #    my_dest=aux[0]+'Im %09d.png'%(auxname)
    #    os.rename(filename, my_dest)
    if not os.path.exists(video_folder + name) and N=="N=0050":
        for filename in glob.glob(folder + '/*.png'):
            img = cv2.imread(filename)
            try:
                height, width, layers = img.shape
                size = (width, height)
                img_array.append(img)
                del (img)
            except:
                pass

        out = cv2.VideoWriter(video_folder + name, cv2.VideoWriter_fourcc(*'DIVX'), 6, size)

        for i in range(len(img_array)):
            out.write(img_array[i])
        out.release()
        del (img_array, out)
        print(name)
