from multiprocessing import Pool
import logging
from Visual_Model import *
logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S ', level=logging.CRITICAL)
def Visual_Algorithm(Constants):
    [Flock, Flock_Colors, Flock_visual_field] = Flock_Create(Constants)
    Flock = np.array(Flock)
    _EXP_TIME = int(5000 /( Constants._DT * Constants._U / Constants._RADIUS))  # time steps
    _HISTORY = _EXP_TIME // 5  # time steps
    TAIL_T=10
    Flock_History = add_metric(np.zeros([int(TAIL_T/( Constants._DT * Constants._U / Constants._RADIUS)), Constants._N, 3]), Flock)
    iterations = _EXP_TIME
    [Center, _] = Flock_Calculate_Center(Flock)
    Metrics=np.zeros([_EXP_TIME,3])
    Metrics[Constants.NewDts,:] = np.array([Flock_Calculate_P(Flock[:, -1], Constants),
                        Flock_Calculate_M(Flock, Center),
                        Flock_Calculate_H(Flock_visual_field, Constants)])
    Flock_Omegas = np.zeros([Constants._N, 3])
    logging.critical("Simulation of: " + str(Constants))
    for i in range(iterations):
        # Move
        Flock = Flock_Update(Flock, Flock_Omegas, Constants)
        logging.debug('Moving')
        # see
        Flock_Past_Visual_Fields = Flock_visual_field
        Flock_visual_field = Flock_Update_Visual_Field(Flock, Flock_Colors, Constants)
        logging.debug('Seeing')
        # calculate
        Flock_Omegas = Flock_Calculate_Omegas(Flock_visual_field, Flock_Past_Visual_Fields, Flock_Omegas, Constants)
        logging.debug('Algorithm')
        # Metrics
        [Center, _] = Flock_Calculate_Center(Flock)
        Metrics[Constants.NewDts, :] = np.array([Flock_Calculate_P(Flock[:, -1], Constants),
                                                 Flock_Calculate_M(Flock, Center),
                                                 Flock_Calculate_H(Flock_visual_field, Constants)])
        if Constants.NewDts -1>= _EXP_TIME-len(Flock_History):
            Flock_History[_EXP_TIME-Constants.NewDts,:] =Flock
        Constants.NewDts += 1

    Flock_Save(Constants, Metrics, _HISTORY, Flock, Flock_Colors, Flock_Omegas)
    Get_Snapshot_at_end(Flock, Metrics, Flock_History, Flock_Colors, Constants)
    logging.critical("Finished..." + str(Constants) + " Final Metrics: " + str(np.mean(Metrics[-_HISTORY:, :], axis=0)))



if __name__ == '__main__':
    _N = 50
    _DT = 0.1
    _U = 1
    Keta = 0.025
    _EPSILON = 1
    MaxAtt=11
    MaxAli=10
    pool = Pool(processes=20)
    for _EPSILON in [1,0]:
        for Qexp in range(5):
            for Keta in [0.01, 0.1]:
                for att in range(0,MaxAtt,2):
                    All_Constants = []
                    for ali in range(1,MaxAli+1):
                        Kodot = att/50
                        Kparallel=ali/50
                        _GAINS = [Kodot, Kparallel,  Keta]
                        _GAINSPretty = [Kodot, Kparallel,  Keta]
                        Constants = Global_static_Variables(_N, _DT, _EPSILON, _GAINS, _U)
                        file_name = os.getcwd() + '\\Sim Files Visual\\' + str(Constants)
                        Path(file_name).mkdir(parents=True, exist_ok=True)
                        All_Constants.append(Constants)
                        Kodot = (1+att) / 50
                        _GAINSPretty = [Kodot, Kparallel,  Keta]
                        _GAINS = [Kodot, Kparallel,  Keta]
                        Constants = Global_static_Variables(_N, _DT, _EPSILON, _GAINS, _U)
                        file_name = os.getcwd() + '\\Sim Files Visual\\' + str(Constants)
                        Path(file_name).mkdir(parents=True, exist_ok=True)
                        All_Constants.append(Constants)
                    Kodot = att / 50
                    Kparallel=0
                    _GAINSPretty = [Kodot, Kparallel,  Keta]
                    _GAINS = [Kodot, Kparallel,  Keta]
                    Constants = Global_static_Variables(_N, _DT, _EPSILON, _GAINS, _U)
                    file_name = os.getcwd() + '\\Sim Files Visual\\' + str(Constants)
                    Path(file_name).mkdir(parents=True, exist_ok=True)
                    Visual_Algorithm(Constants)
                    Kodot = (1+att) / 50
                    _GAINSPretty = [Kodot, Kparallel,  Keta]
                    _GAINS = [Kodot, Kparallel,  Keta]
                    Constants = Global_static_Variables(_N, _DT, _EPSILON, _GAINS, _U)
                    file_name = os.getcwd() + '\\Sim Files Visual\\' + str(Constants)
                    Path(file_name).mkdir(parents=True, exist_ok=True)
                    Visual_Algorithm(Constants)
                    array_2D = np.array(pool.map(Visual_Algorithm, All_Constants))

    pool.close()  # ATTENTION HERE

