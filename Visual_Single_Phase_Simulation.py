from Visual_Model import *
import numpy as np
import logging

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%Y/%m/%d %H:%M:%S ', level=logging.CRITICAL)
if __name__ == '__main__':
    _N=50
    _DT = 0.1
    _U=1
    Kodot=0.0
    Kparallel=0.2
    Keta = 0.00
    _EPSILON=1
    _GAINS= [Kodot,Kparallel,Keta]
    Constants=Global_static_Variables(_N, _DT, _EPSILON, _GAINS, _U)
    [Flock,Flock_Colors,Flock_visual_field]=Flock_Create(Constants)
    Flock=np.array(Flock)
    _EXP_TIME = int(15000/ (Constants._DT * Constants._U / Constants._RADIUS))  # time steps
    _HISTORY = _EXP_TIME // 5  # time steps
    TAIL_T = 10
    Flock_History = add_metric(np.zeros([int(TAIL_T / (Constants._DT* Constants._U) ), Constants._N, 3]), Flock)
    _LOOP_COUNTER=0
    Icontinue=True
    iterations=_EXP_TIME
    [Center,_]=Flock_Calculate_Center(Flock)
    Metrics=np.array([Flock_Calculate_P(Flock[:,-1],Constants),
                      Flock_Calculate_M(Flock,Center),
                      Flock_Calculate_H(Flock_visual_field,Constants)])
    Flock_Omegas=np.zeros([_N,3])
    logging.critical("Simulation of: "+str(Constants))
    while Icontinue:
        for i in range(iterations):
            #Move
            Flock=Flock_Update(Flock,Flock_Omegas,Constants)
            logging.debug('Moving')
            #see
            Flock_Past_Visual_Fields=Flock_visual_field
            Flock_visual_field=Flock_Update_Visual_Field(Flock,Flock_Colors,Constants)
            logging.debug('Seeing')
            #calculate
            Flock_Omegas=Flock_Calculate_Omegas(Flock_visual_field,Flock_Past_Visual_Fields,Flock_Omegas,Constants)
            logging.debug('Algorithm')
            #Metrics
            [Center,_]=Flock_Calculate_Center(Flock)
            dtMetrics=np.array([Flock_Calculate_P(Flock[:,-1],Constants),
                      Flock_Calculate_M(Flock,Center),
                      Flock_Calculate_H(Flock_visual_field,Constants)])
            logging.debug('Metrics')
            #logging.critical(np.mean(Flock_Omegas,axis=0))
            if len(Metrics.shape)==1:
                Metrics=np.array([Metrics,dtMetrics])
            else:
                Metrics=np.append(Metrics,[dtMetrics],axis=0)
            Flock_History=add_metric(Flock_History,Flock)
            if Constants.NewDts%100==0:
                Get_Snapshot_at_dt(Flock,Metrics,Flock_History,Flock_Colors,Constants)
                Flock_Save_inst(Constants, Metrics, _HISTORY, Flock, Flock_Colors, Flock_Omegas)
                logging.critical(str(Constants.NewDts*Constants._U)+" Current Metrics: " + str(np.mean(Metrics[-_HISTORY:, :], axis=0)))
                #print(Constants._DT * np.sum(Constants._GAINS*Flock_Omegas[1,:],axis=1, keepdims=True))
            Constants.NewDts+=1

        _LOOP_COUNTER+=1
        logging.critical(str(iterations) +' dt completed')
        Metrics_Variance=np.mean(np.power(Metrics[-_HISTORY:,:-1]-np.mean(Metrics[-_HISTORY:,:-1],axis=0),2),axis=0)
        logging.critical("Current Metrics Variance: "+str(Metrics_Variance))
        logging.critical("Current Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))
        if (Metrics_Variance>1/np.sqrt(Constants._DT)/100).any():
            iterations=_HISTORY
        else:
            iterations=-1
            Icontinue=False
        if _LOOP_COUNTER>30 and np.sum((Metrics_Variance<1/np.sqrt(Constants._DT)/100)*1)>=2:
            iterations=0
            Icontinue=False
        if len(Metrics) >= 6*_EXP_TIME:
            iterations=-2
        Icontinue=False
        if Icontinue:
            logging.warning("Yet to Converge... Current Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))
    logging.critical("Finishing... Final Variance: "+str(Metrics_Variance)+ " Target: "+str(2/np.sqrt(Constants._DT)/100))
    if iterations==-1:
        logging.critical("Successful "+str(Constants)+" Final Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))
    elif iterations==0:
        logging.critical("One Metric unstable "+str(Constants)+" Final Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))
    elif iterations==-2:
        logging.critical("Too Long not stable "+str(Constants)+" Final Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))
    else:
        logging.critical("MaxSize "+str(Constants)+" Final Metrics: "+str(np.mean(Metrics[-_HISTORY:,:],axis=0)))